package com.thundersquared.blodder;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by anatoli on 20/02/15.
 */
public class BodePlotter extends SurfaceView implements SurfaceHolder.Callback {
	private static int paths_num = 0;

	ArrayList<Pair<Path, Paint>> paths = new ArrayList<Pair<Path, Paint>>();

	// Constructors
	public BodePlotter(Context context) {
		super(context);
		//init();
	}
	public BodePlotter(Context context, AttributeSet attrs) {
		super(context, attrs);
		//init();
	}
	public BodePlotter(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//init();
	}

	// Init
	public void init() {
		// Add path
		init(paths_num);
		paths_num++;
	}
	public void init(int index) {
		// Set up Paint
		Paint paint = new Paint();
		paint.setStrokeWidth(10);
		paint.setDither(true);                    // set the dither to true
		paint.setStyle(Paint.Style.STROKE);       // set to STROKE
		paint.setAntiAlias(true);
		// Set up path
		Path path = new Path();
		// Add them to the list
		paths.add(new Pair<>(path, paint));
		// Change things based on the index
		switchEffect(index, false);
		setColor(index, Color.BLUE);
	}

	// Start path from point
	public void startFrom(int index, float x, float y) {
		// Set starting point
		// Get paths at index and select path (first) then move to point
		paths.get(index).first.moveTo(x, y);
		invalidate();
	}

	// Drive path to point
	public void goTo(int index, float x, float y) {
		// Get paths at index and select path (first) then line to point
		paths.get(index).first.lineTo(x, y);
		invalidate();
	}

	// Set path color
	public void setColor(int index, int color) {
		paths.get(index).second.setColor(color);
	}

	// Set path squared or curvy
	public void switchEffect(int index, boolean corners) {
		if (corners == true) {
			paths.get(index).second.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
			paths.get(index).second.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
			paths.get(index).second.setPathEffect(new CornerPathEffect(100) );   // set the path effect when they join.
		} else {
			paths.get(index).second.setStrokeJoin(Paint.Join.BEVEL);    // set the join to round you want
			paths.get(index).second.setStrokeCap(Paint.Cap.SQUARE);      // set the paint cap to round too
			paths.get(index).second.setPathEffect(null);   // set the path effect when they join.
		}
	}

	// Clear canvas paths
	public void clearCanvas() {
		paths.clear();
		paths_num = 0;
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);

		// Paint single line
		//canvas.drawPath(path, paint);

		// Paint the whole array (multiple lines)
		for (Pair<Path, Paint> p : paths) {
			canvas.drawPath(p.first, p.second);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		setWillNotDraw(false); //Allows us to use invalidate() to call onDraw()
		postInvalidate();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {}

}
