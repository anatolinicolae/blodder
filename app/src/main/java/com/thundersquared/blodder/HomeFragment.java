package com.thundersquared.blodder;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;


public class HomeFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private Switch circuit_sw;
	private Button plot_btn, clear_btn;
	private BodePlotter graph_view;
	private EditText rin, cin;

	/**
	 * Returns a new instance of this fragment for the given section
	 * number.
	 */
	public static HomeFragment newInstance(int sectionNumber) {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public HomeFragment() {

	}

	// Fragment created and running
	@Override
	public void onResume() {
		super.onResume();

		// Get switch and attach listener
		circuit_sw = (Switch) getView().findViewById(R.id.circuit_sw);
		circuit_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					circuit_sw.setText("CR Circuit (Highpass)");
				} else {
					circuit_sw.setText("RC Circuit (Lowpass)");
				}
			}
		});

		rin = (EditText) getView().findViewById(R.id.resistor_val);
		cin = (EditText) getView().findViewById(R.id.capacitor_val);

		// Get graph widget
		graph_view = (BodePlotter) getView().findViewById(R.id.bode_graph);
		graph_view.clearCanvas();

		// Get "Plot" button and connect listener to plot something
		plot_btn = (Button) getView().findViewById(R.id.plot_btn);
		plot_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				graph_view.clearCanvas();

				graph_view.init();
				int red = getResources().getColor(R.color.primaryVividRed);
				graph_view.setColor(0, red);
				//graph_view.startFrom(0, 200.0f, 200.0f);
				//graph_view.goTo(0, 300.0f, 200.0f);

				double  rv = Double.parseDouble(rin.getText().toString()),
						cv = Double.parseDouble(cin.getText().toString());

				double  w = 2 * Math.PI * 1,
						tf = 1 / (1 + w * rv * cv),
						y = (Math.log(1000-tf*1000) / Math.log(20) ) * 100;
				graph_view.startFrom(0, 100, (float) y);

				for (int i = 10; i < 15000; i = (int) (i * 1.5)) {
					w = 2 * Math.PI * i;
					tf = 1 / (1 + w * rv * cv);
					y = (Math.log(1000-tf*1000) / Math.log(20) ) * 100;
					graph_view.goTo(0, (float) (100 + Math.log(i)*50), (float) y);
				}

				/*graph_view.init();
				graph_view.switchEffect(1, true);
				int cyan = getResources().getColor(R.color.primaryVividCyan);
				graph_view.setColor(1, cyan);

				graph_view.startFrom(1, 600.0f, 200.0f);
				graph_view.goTo(1, 700.0f, 200.0f);
				graph_view.goTo(1, 800.0f, 400.0f);*/
			}
		});

		// Get "Clear" button an connect listener to clean the canvas
		clear_btn = (Button) getView().findViewById(R.id.clear_btn);
		clear_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				graph_view.clearCanvas();
			}
		});
	}

	// Inflating fragment in the container
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_home, container, false);
	}

	// Attaching fragment to the view
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(
				getArguments().getInt(ARG_SECTION_NUMBER));
	}
}
