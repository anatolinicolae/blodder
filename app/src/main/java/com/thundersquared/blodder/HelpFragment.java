package com.thundersquared.blodder;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HelpFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private WebView help_browser;

	/**
	 * Returns a new instance of this fragment for the given section
	 * number.
	 */
	public static HelpFragment newInstance(int sectionNumber) {
		HelpFragment fragment = new HelpFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public HelpFragment() {
	}

	@Override
	public void onResume() {
		super.onResume();

		// Get WebView
		help_browser = (WebView) getView().findViewById(R.id.help_browser);

		// Enable JS
		WebSettings webSettings = help_browser.getSettings();
		webSettings.setJavaScriptEnabled(true);

		// Force opening site inside app
		help_browser.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});

		// Go to URL
		help_browser.loadUrl("http://peano.it");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_help, container, false);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(
				getArguments().getInt(ARG_SECTION_NUMBER));
	}
}
